﻿using System;
using System.Collections.ObjectModel;
using System.Xml;
using System.Xml.Linq;

namespace RPD.DAL {
	/// <summary>
	/// Описание конфигурации connection point'ов
	/// </summary>
	public class ConnectionPointsDescription {
		/// <summary>
		/// Список групп описания (конфигруации)
		/// </summary>
		public ObservableCollection<ConnectionPointsGroup> Groups { get; private set; }

		/// <summary>
		/// Инициализирует новый экземпляр класса
		/// </summary>
		public ConnectionPointsDescription() {
			Groups = new ObservableCollection<ConnectionPointsGroup>();
		}

		/// <summary>
		/// Загрузить из файла
		/// </summary>
		/// <param name="filename">Путь к файлу</param>
		public void Load(string filename) {
			if (filename == null) throw new ArgumentNullException("filename");
			try {
				var document = new XmlDocument();
				document.Load(filename);
				Groups.Clear();
				foreach (XmlNode node0 in document.ChildNodes) {
					if (node0.Name == "ConnetionPointsDescriprion") {
						foreach (XmlNode node1 in node0.ChildNodes) {
							if (node1.Name == "Group") {
								var group = new ConnectionPointsGroup(node1.Attributes["Name"].Value);
								foreach (XmlNode node2 in node1.ChildNodes) {
									if (node2.Name == "Point") {
										var point = new ConnectionPoint(node2.Attributes["Name"].Value);
										point.Condition.IsUsed = bool.Parse(node2.Attributes["IsUsed"].Value);
										point.Condition.ConnectionPointIndex = int.Parse(node2.Attributes["ConnectionPointIndex"].Value);

										point.Condition.ControlValue = int.Parse(node2.Attributes["ControlValue"].Value);
										point.Condition.UseControlValue = bool.Parse(node2.Attributes["UseControlValue"].Value);

										point.Condition.HighLimit = int.Parse(node2.Attributes["HighLimit"].Value);
										point.Condition.UseHighLimit = bool.Parse(node2.Attributes["UseHighLimit"].Value);

										point.Condition.LowLimit = int.Parse(node2.Attributes["LowLimit"].Value);
										point.Condition.UseLowLimit = bool.Parse(node2.Attributes["UseLowLimit"].Value);

										point.Condition.UseValueAbs = bool.Parse(node2.Attributes["UseValueAbs"].Value);
										point.Condition.UseLogicalAnd = bool.Parse(node2.Attributes["UseLogicalAnd"].Value);

										group.Points.Add(point);
									}
								}
								Groups.Add(group);
							}
						}
					}
				}
			}
			catch(Exception ex) {
				throw new Exception("Не удалось загрузить описание конфигураци точки соединения из файла " + filename, ex);
			}
		}

		/// <summary>
		/// Сохранить в файл
		/// </summary>
		/// <param name="filename">Путь к файлу</param>
		public void Save(string filename) {
			if (filename == null) throw new ArgumentNullException("filename");
			try {
				var document = new XElement("ConnetionPointsDescriprion");

				foreach (ConnectionPointsGroup group in this.Groups) {
					var groupNode = new XElement("Group", new XAttribute("Name", group.Name));

					foreach (ConnectionPoint point in group.Points) {
						var pointNode = new XElement("Point",
							new XAttribute("IsUsed", point.Condition.IsUsed),
							new XAttribute("ConnectionPointIndex", point.Condition.ConnectionPointIndex),
							new XAttribute("ControlValue", point.Condition.ControlValue),
							new XAttribute("UseControlValue", point.Condition.UseControlValue),
							new XAttribute("HighLimit", point.Condition.HighLimit),
							new XAttribute("UseHighLimit", point.Condition.UseHighLimit),
							new XAttribute("LowLimit", point.Condition.LowLimit),
							new XAttribute("UseLowLimit", point.Condition.UseLowLimit),
							new XAttribute("UseValueAbs", point.Condition.UseValueAbs),
							new XAttribute("UseLogicalAnd", point.Condition.UseLogicalAnd),
							new XAttribute("Name", point.Name));
						groupNode.Add(pointNode);
					}
					document.Add(groupNode);
				}
				document.Save(filename);
			}
			catch (Exception ex)
			{
				throw new Exception("Не удалось сохранить описание конфигураци точки соединения из файла " + filename, ex);
			}
		}
	}
}
