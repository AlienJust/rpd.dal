﻿namespace RPD.DAL {
	/// <summary>
	/// Represents one connection point
	/// </summary>
	public class ConnectionPoint {
		private readonly IDumpCondition _condition;
		private string _name;

		/// <summary>
		/// Инициализирует новый экземпляр класса
		/// </summary>
		/// <param name="name">Название будущей точки</param>
		public ConnectionPoint(string name) {
			_name = name;
			_condition = new RpdChannellDumpCondition();
		}

		/// <summary>
		/// Условие для этой точки
		/// </summary>
		public IDumpCondition Condition
		{
			get { return _condition; }
		}

		/// <summary>
		/// Название ConnectionPoint'а
		/// </summary>
		public string Name
		{
			get { return _name; }
			set { _name = value; } // TODO: do I need setter?
		}
	}
}
