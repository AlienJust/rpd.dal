﻿using System.Collections.ObjectModel;

namespace RPD.DAL {
	/// <summary>
	/// Репрезентс группу connection point'ов
	/// </summary>
	public class ConnectionPointsGroup {
		/// <summary>
		/// Название группы точек
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Список точек данной группы
		/// </summary>
		public ObservableCollection<ConnectionPoint> Points { get; private set; }

		/// <summary>
		/// Инициализирует новый экземпляр класса
		/// </summary>
		/// <param name="name">Название группы</param>
		public ConnectionPointsGroup(string name) {
			Name = name;
			Points = new ObservableCollection<ConnectionPoint>();
		}
	}
}
