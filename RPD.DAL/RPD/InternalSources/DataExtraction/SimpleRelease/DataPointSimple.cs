﻿using System;

namespace RPD.DAL.DataExtraction.SimpleRelease {
	internal sealed class DataPointSimple : IDataPoint
	{
		private readonly double _value;
		private readonly DateTime _time;
		private readonly bool _valid;
		private readonly ulong _dataPosition;

		public DataPointSimple(double value, DateTime time, bool valid, ulong dataPosition)
		{
			_value = value;
			_time = time;
			_valid = valid;
			_dataPosition = dataPosition;
		}

		public double Value
		{
			get { return _value; }
		}

		public DateTime Time
		{
			get { return _time; }
		}


		public bool Valid
		{
			get { return _valid; }
		}

		public ulong DataPosition
		{
			get { return _dataPosition; }
		}
	}
}