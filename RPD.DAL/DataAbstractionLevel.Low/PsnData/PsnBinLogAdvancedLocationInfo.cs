using System;

namespace DataAbstractionLevel.Low.PsnData {
	internal struct PsnBinLogAdvancedLocationInfo {
		public readonly string PsnBinFileName;
		private PsnPageNumberAndTime _firstPageInfo;
		private PsnPageNumberAndTime _lastPageInfo;

		private PsnPageNumberAndTime? _firstDatedPageInfo;
		private bool _isLastPsnLogOnDevice;

		public PsnBinLogAdvancedLocationInfo(string fileName, PsnPageNumberAndTime firstPageInfo, PsnPageNumberAndTime lastPageInfo) {
			PsnBinFileName = fileName;
			_isLastPsnLogOnDevice = false;

			_firstPageInfo = firstPageInfo;
			_lastPageInfo = lastPageInfo;
	
			_firstDatedPageInfo = null;
		}

		public void UpdateFirstDatedPageInfo(PsnPageNumberAndTime? info) {
			_firstDatedPageInfo = info;
		}

		public bool IsLastPsnLogOnDevice {
			get { return _isLastPsnLogOnDevice; }
			set { _isLastPsnLogOnDevice = value; }
		}

		public PsnPageNumberAndTime FirstPageInfo {
			get { return _firstPageInfo; }
		}

		public PsnPageNumberAndTime? FirstDatedPageInfo
		{
			get { return _firstDatedPageInfo; }
		}

		public PsnPageNumberAndTime LastPageInfo
		{
			get { return _lastPageInfo; }
		}

		public void UpdateFirstPageInfoTime(DateTime? time) {
			_firstPageInfo.Time = time;
		}

		public void UpdateLastPageInfoTime(DateTime? time)
		{
			_lastPageInfo.Time = time;
		}
	}
}